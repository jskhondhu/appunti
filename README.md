# appunti
il modo più pulito


# risposta agli incidenti
SANS Incident Response stages

Preparation. Identification. Containment. Eradication. Recovery. Lessons Learned.

Standard Operating Procedures, or SOPs.

A runbook is a collection of organized SOPs.

> IR SOP for Phishing

Preparation: Have (this) SOP up to date and know where the needful tools and training are to work through this SOP.

Identification: ID the email, sender and (many) recipient, check the email header, check the true sender, check the URIs and redirects, check the files within are safe.

Containment: If there is a malicious nature that can be seen, respond to person/s who raised this, with next steps, check if person/s clicked on any links, and look at ingress and egress traffic, as well as local host intrusion signals.

Eradication: Pull/delete all emails ourselves, using <this> method/automation. Take the person/s off the network whilst this is being worked on to stop spread. Blocked domains.

Recovery: Respond to person/s, thanks for raising this, this is what was found and how we acted and what we have done to be be proactive going forward.

Lessons Learned: How it could of been done more effeciently better next time. Update detect rules? Update black, whitelists.

> IR SOP for Malware

Preparation: Have (this) SOP up to date and know where the needful tools and training are to work through this SOP.

Identification: 

Containment: 

Eradication: 

Recovery: 

Lessons Learned: 



# forense
Informatica forense

```
Acquire Image/memory (with sound integrity)
Calculate hashes (MD5, SHA1, SHA2)
Analyse with questions wanting to be answered in mind/set goals
Document findings
```

> Acquiring an image
```
FTK Imager
Paladin
linux dd
```

```
FTK Imager from AccessData
FTK Imager is a Windows acquisition tool included in various forensics toolkits such as SANS SIFT.
FTK Imager does not guarantee data is not written to the drive, so it is important to use a write blocker (Tableau?)

Use a HDD Dock if the HDD/Drive is dead.

FTK Imager.exe
File > Create a Disk Image > Select Source (Physical) > Export > Select Destination (ie Ext HDD)
(If you select raw (dd) format, the image meta data will not be stored in the image file itself)
Image type > Raw (dd) | SMART | EO1 > Fill in case details > Select the Image Destination folder and file name > Finish
E01 because of its hash verification and its ability to used with multiple other programs.
..
Process them with autopsy or boot the .E01 into a Virtual Machine with VMware
..

```
```
Sumuri Paladin
Run from portable USB (https://sumuri.com/make-your-own-paladin-usb-2/)
You will need Unetbootin (https://unetbootin.github.io). The drive will need to be formatted to FAT32 before using Unetbootin.
Boot from USB (Paladin 7?) 
```
```
dd
Disk sectors are 512 bytes, so (calculate) sector value x 512 will give us the correct byte offset to be using.
dd if=/dev/hdb of=/image.img

if input file
bs block size
skip start off set in blocks
count how many blocks
of output file
```
> Acquiring memory
```
FTK Imager

```
```
FTK Imager from AccessData
FTK Imager.exe
File > Capture memory > Select destination > Capture memory
Once complete > Close.
```



Windows


Linux

exiftool

ewfinfo

fdisk

xxd


Mac

plutil
Property list utility
/user/Library/Preferences/*.plist
plutil -p something.plist

hdiutil
Manipulate disk images
hdiutil imageinfo /Users/james/Houseparty.dmg

Analisi forensi della rete

> Using the provided PCAP, what is the HTTP URL of the host being contacted in the TCP stream associated with the first packet in the capture? Example: http://somesite.com
```
tshark -r file.pcap -T fields -e http.request.full_uri | uniq | head -10
```

> Using the PCAP file provided, what is the IP address of the system involved in all of the IPv4 conversations? Example: 1.2.3.4
```
tshark -r file.pcap -T fields -e ip.dst -e ip.src -e eth.dst -e eth.src | sort | uniq
```


# analisi del malware
```
file file.exe
```
```
strings file.exe
```
md5
Message-digest algorithm is a widely used hash function producing a 128-bit hash value
```
md5 file.exe
```
shasum
Print or Check SHA Checksums
```
shasum 
shasum -a256 
```

hexdump
Hexadecimal view of computer data
```
hexdump -C file.exe
```
upx

Volatility
An advanced memory forensics framework
Reference guide (https://github.com/volatilityfoundation/volatility/wiki/Command-Reference)

Working with a Win 10 mem dump called. memdump.mem:
```
vol.py -f memdump.mem imageinfo
Suggested Profile(s) : Win10x64_17134, Win10x64_14393, Win10x64_10586, Win10x64_16299, Win2016x64_14393, Win10x64_17763, Win10x64_15063 (Instantiated with Win10x64_15063)
```

> Get/Find the rogue (malicious) process

```
vol.py -f memdump.mem --profile=Win10x64_17134 pstree
. 0xffffc20c69d00580:userinit.exe                    4756    732      0 ------ 2018-08-01 19:20:57 UTC+0000
.. 0xffffc20c69cfe580:explorer.exe                   4824   4756    125      0 2018-08-01 19:20:58 UTC+0000
... 0xffffc20c6ddad580:svchost.exe                   8560   4824     10      0 2018-08-01 20:13:10 UTC+0000
... 0xffffc20c6e495080:cmd.exe                       8868   4824      0 ------ 2018-08-01 19:40:14 UTC+0000
... 0xffffc20c6cdf4580:scvhost.exe                    360   4824      0 ------ 2018-08-01 19:56:45 UTC+0000
... 0xffffc20c6ab70080:svchost.exe                   8852   4824      0 ------ 2018-08-01 19:59:49 UTC+0000
... 0xffffc20c6c095580:MSASCuiL.exe                  6268   4824      3      0 2018-08-01 19:21:56 UTC+0000
```

svchost should (always) come from parent %SystemRoot%\system32\services.exe which should come from %SystemRoot%\system32\wininit.exe.

It will always follow the command line convention of “svchost.exe -k [name]”.

%SystemRoot%\system32\wininit.exe creates Services.exe, starts Lsass.exe, starts Lsm.exe, always a child of SMSS.EXE (We know this won’t have a Parent process due to smss termination after running)
Creates %windir%\temp, runs within session 0.

As with SMSS.EXE, userinit.exe exits after it has run, meaning anything loaded from registry (explorer.exe, dodgy malwares) will not have a parent process. It also means userinit.exe will not be visible to you in Process Explorer.

Svchost did not derive from services but explorer.exe 4824. Note column three above is thread usage.

> Find the running rogue (malicious) process and dump its memory to disk.

```
vol.py -f memdump.mem --profile=Win10x64_17134 memdump -p 4824 --dump-dir=./
```
```
vol.py -f memdump.mem --profile=Win10x64_17134 procdump -p 4824 --dump-dir=./
```
```
file 4824.exe
shasum -a256 4824.exe
```
```
vol.py -f memdump.mem --profile=Win10x64_17134 netscan
```

Baseline a good mem image versus a potentially bad one:
```
vol.py -f memdump.mem --profile=Win10x64_18362 processbl -B clean.mem -U 2>/dev/null
```

> Get the MD5 hash
```
md5 file
```
> Get the SHA1 hash
```
shasum file
```
> Get the SHA2 hash
```
shasum -a256 file
```
> Get a particular string from a particular decimal offset of said file.
```
Convert dec to hex:
printf '%x\n' 68936
10d48
hexdump -C file.exe | grep 10d4
4d 6f 7a 69 6c 6c 61 2f Mozilla/
```
> What type of file is it?
```
file file.exe 
file.exe: PE32 executable (GUI) Intel 80386, for MS Windows
GUI VS CONSOLE VS DLL.
```
> What is the hex value at offset 0x00011556 inside the file and its ASCII equivalent
```
hexdump -C file.exe | grep 0001155
57 49  4e 49 4e 45 54 2e 64 6c  WININET.dl
```
> Get the Mal file PE sections
```
file file.exe
shasum -a256 file.exe
strings p.exe | head -20
Section names, .text, .rdata, .data, .rsrc, .reloc, 5 to the count here.

In VT under /details under section Portable Executable Info, subsection sections.
eg, https://www.virustotal.com/gui/file/f5ff02f7ee24526d7a05a6b66f85143b1cbbb8f75bea7d2a08bedf09592e3efb/details
Section names, .text, .rdata, .data, .rsrc, .reloc, 5 to the count here.
```



#informazioni sulla minaccia informatica
ISC
IBM XForce
Cisco 
Twitter
Threatconnect
Silobreaker




# something


